var gulp        = require('gulp'),
    uglify      = require('gulp-uglifyjs'),
    cssnano     = require('gulp-cssnano'),
    rename      = require('gulp-rename'),
    imagemin    = require('gulp-imagemin'),
    pngquant    = require('imagemin-pngquant');


gulp.task('scripts', function() {
  return gulp.src('static/js/script.js')
      .pipe(uglify())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('static/js'));
});

gulp.task('minify-normalize-css', function() {
  return gulp.src('static/css/normalize.css')
      .pipe(cssnano())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('static/css'));
});

gulp.task('minify-style-css', function() {
  return gulp.src('static/css/style.css')
      .pipe(cssnano())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('static/css'));
});

gulp.task('opt-images', function() {
  return gulp.src('static/images/**/*')
      .pipe(imagemin({
        interlaced: true,
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()]
      }))
      .pipe(gulp.dest('static/images'));
});

gulp.task('build', ['minify-normalize-css', 'minify-style-css', 'scripts', 'opt-images'], function() {});
