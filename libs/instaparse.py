#!/usr/local/bin/python3

from bs4 import BeautifulSoup as bs
import requests

def get_data(link):
    '''Returns links to image and video(if exists) and message, e.g. 404 if there is  a wrong link'''
    page = requests.get(link)
    if (page.status_code == 404):
        return (None, None, 'notfound')
    page.encoding = 'utf-8'
    soup = bs(page.text,'html.parser')
    video = soup.find('meta', attrs={'property': 'og:video', 'content': True})
    img = soup.find('meta', attrs={'property': 'og:image', 'content': True})
    if (video != None):
        video = video['content'].split('?')[0]
    img = img['content'].split('?')[0]
    return (img, video, '200')

# i = 'https://www.instagram.com/p/BPbmL9IgG6t'
# v = 'https://www.instagram.com/p/BPoRtDrA4WB/'
