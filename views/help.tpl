<div class="block1">
  <div class="num"><p>1</p></div>
  <img src="/static/images/b1.jpg" alt="{{text['block1_alt']}}" title="{{text['block1_alt']}}">
  <p>{{text['block1']}}</p>
</div>
<div class="block2">
  <div class="num"><p>2</p></div>
  <img src="/static/images/b2.jpg" alt="{{text['block2_alt']}}" title="{{text['block2_alt']}}">
  <p>{{text['block2']}}</p>
</div>
<div class="block3">
  <div class="num"><p>3</p></div>
  <img src="/static/images/b3.jpg" alt="{{text['block3_alt']}}" title="{{text['block3_alt']}}">
  <p>{{text['block3']}}</p>
</div>
<div class="blokhow">
  <div class="how"><h3>{{text['how_title_1']}}</h3>{{text['how_text_1']}}</div>
  <div class="how"><h3>{{text['how_title_2']}}</h3>{{text['how_text_2']}}</div>
  <div class="how"><h3>{{text['how_title_3']}}</h3>{{text['how_text_3']}}</div>
  <div class="how"><h3>{{text['how_title_4']}}</h3>{{text['how_text_4']}}</div>
</div>
