<!DOCTYPE html>
<html>
  <head lang="{{text['lang']}}">
    <meta charset="utf-8">
    <title>{{text['title']}}</title>
    <meta name="description" content="{{text['description']}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
		<link rel="stylesheet" href="/static/css/normalize.min.css">
		<link rel="stylesheet" href="/static/css/style.min.css">
    <link rel="icon" href="/static/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/static/favicon.ico" type="image/x-icon">
    <meta property="og:title" content="{{text['title']}}">
    <meta property="og:image" content="http://downloadfrominstagram.org/static/images/ogimg.png">
    <meta property="og:url" content="http://downloadfrominstagram.org/">
    <meta property="og:description" content="{{text['description']}}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script type="text/javascript" src="/static/js/script.min.js"></script>
    <link rel="canonical" href="http://downloadfrominstagram.org/">
    <link rel="alternate" href="http://downloadfrominstagram.org/" hreflang="x-default"/>
    <link rel="alternate" href="http://downloadfrominstagram.org/" hreflang="en"/>
    <link rel="alternate" href="http://downloadfrominstagram.org/ru/" hreflang="ru"/>
  </head>
  <body>
    <div class="site">
      <header>
        <div class="line">
          <a href="{{text['homepage']}}" alt="Home Page" title="Home Page">
            <div class="logo">
              <h1>DownloadFromInstagram.org</h1>
            </div>
          </a>
          <div class="language">
            <select id="language" class="select" onchange="changeLang();">
              <option value="ru" {{text.get('ru','')}}>Russian</option>
              <option value="en" {{text.get('en','')}}>English</option>
            </select>
          </div>
        </div>
      </header>
      <div class="download">
        %if show_ads:
          <div class="ads1">
            <noindex><a href="http://c.cpl11.ru/dNCN" target="_blank" rel="nofollow"><img src="/static/images/la2.jpg"></a></noindex>
          </div>
        %end
        %if (not msg) or (show_ads):
        <div class="textdown">
          <h1>{{text['slogan']}}</h1>
        </div>
        %end
        <form id="form" onsubmit="return CheckLink();" class="input-form" action="{{text['form_action']}}" method="post">
          <span>{{text['label_input_link']}}</span>
          <input id="link-src" class="link" type="text" name="link-src" placeholder="{{text['placeholder']}}"/>
          <input class="button" type="submit" name="submit" value="{{text['btn_go']}}">
        </form>
        <div id="wrong-link" class="error wl_hide">{{text['wronglink']}}</div>
      </div>
      <div class="main">
        %if not msg:
          %include('help.tpl')
        %else:
          <div class="blockdown">
            %if msg == 'notfound':
              %include('not_found.tpl')
            %elif msg == '404':
              %include('error404.tpl')
            %else:
              %include('found.tpl')
            %end
          </div>
        %end
      </div>
    </div>
    <footer>
      2017 &#169; DownloadFromInstagram.org
      <!-- GoogleAnalytics -->
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-91958706-1', 'auto');
        ga('send', 'pageview');
      </script>
      <!-- Addthis tools -->
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58a6ed0794edc41e"></script>
    </footer>
  </body>
</html>
