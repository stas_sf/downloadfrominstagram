% link, btn_text = (video, text['btn_download_vid']) if video else (image, text['btn_download_img'])
<img src={{image}}>
<p>
  <a href={{link}}?dl=1>{{btn_text}}</a>
</p>
