#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

from libs.bottle import *
from libs.instaparse import get_data
import re
import os
import json
import requests
os.chdir(os.path.dirname(__file__)) # comment to run on local

#######################
### Global variables
#######################

languages = {'en': 'static/json/en.json', 'ru': 'static/json/ru.json', 'by': 'static/json/ru.json',
             'ua': 'static/json/ru.json'}
show_ads = True

#######################
### Support functions
#######################

def get_user_lang():
    """ Check user language by several ways:
            First, if there is cookie the one is prior
            Second, check browser lang
            Third, check language by IP via service 'geoplugin.net'
    """
    lang = request.get_cookie('lang')
    if not lang:
        if 'HTTP_ACCEPT_LANGUAGE' in request.environ.keys():
            lang = request.environ['HTTP_ACCEPT_LANGUAGE'].split('-')[0]
        elif 'HTTP_REMOTE_ADDRESS' in request.environ.keys():
            ip = request.environ['HTTP_REMOTE_ADDRESS']
            jsn = json.loads(requests.get('http://www.geoplugin.net/json.gp?ip='+ip).text)
            lang = jsn['geoplugin_countryCode'].lower()
        else:
            lang = 'en'
        response.set_cookie('lang', lang, path='/')
    return lang

def load_json(lang=None):
    """Load json file with text"""
    if not lang:
        lang = get_user_lang()
    if lang not in languages.keys():
        lang = 'en'
    with open(languages[lang], encoding='utf-8') as f:
        return json.load(f)

def is_mobile():
    """Check if user-agent is mobile. If true, set show_ads True"""
    if 'HTTP_USER_AGENT' in request.environ.keys():
        user_agent = request.environ['HTTP_USER_AGENT']
        return not (user_agent.find('Mobile') != -1 or user_agent.find('iPhone') != -1 or user_agent.find('Android') != -1)
    else:
        return False

def is_bot():
    """Check if user-agent is bot"""
    if 'HTTP_USER_AGENT' in request.environ.keys():
        user_agent = request.environ['HTTP_USER_AGENT']
        return user_agent.lower().find('bot') == -1
    else:
        return False

def change_lang():
    """Check if there is cookie change_lang.
    It's used when language is changed by user.
    Return True if language should be selected by program.
    Return False if language should be selected from url.
    """
    return request.get_cookie('change_lang') == 'True'

@route('/static/<filename:path>')
def server_static(filename):
    return static_file(filename, root='./static')


#######################
### Main pages with English language
#######################

@route('/')
@view('page')
def index():
    if change_lang():
        lang = get_user_lang()
        response.set_cookie('change_lang', 'False', path='/')
        if lang != 'en':
            return redirect('/'+lang+'/')
    else:
        lang = 'en'
    response.set_cookie('lang', 'en', path='/')
    text = load_json(lang)
    show_ads = is_mobile()
    return dict(image=None, video=None, msg=None, show_ads=show_ads, text=text)


@post('/')
@view('page')
def dld():
    """ Search and return page with image (video) or error message.
    """
    link_src = request.forms.get('link-src')
    link_src = re.search(r'instagram\.com/(p/)?([a-zA-Z]|[0-9]|[_-])+', link_src)
    if link_src != None:
        link_src = link_src.group(0)
        #lang = get_user_lang()
    # else:
    #     lang = get_user_lang()
    #     text = load_json(lang)
    #     show_ads = is_mobile()
    #     return dict(image=None, video=None, msg=None, show_ads=show_ads, text=text)
    link = link_src.split('/')[-1]
    return redirect('/download/'+link)


@post('/download/<url>')
@post('/download/<url>/')
@get('/download/<url>')
@get('/download/<url>/')
@view('page')
def download(url):
    if change_lang():
        response.set_cookie('change_lang', 'False', path='/')
        lang = get_user_lang()
        if lang != 'en':
            return redirect('/'+lang+'/download/'+url)
    else:
        lang = 'en'
    response.set_cookie('lang', 'en', path='/')
    link_src = 'https://instagram.com/p/'+url
    img, video, msg = get_data(link_src)
    text = load_json(lang)
    show_ads = is_mobile()
    return dict(image=img, video=video, msg=msg, show_ads=show_ads, text=text)

#######################
### Other languages
#######################

@route('/<lang:re:[a-z][a-z]>')
@route('/<lang:re:[a-z][a-z]>/')
@view('page')
def index_lang(lang):
    if change_lang():
        response.set_cookie('change_lang', 'False', path='/')
        return redirect('/')
    response.set_cookie('lang', lang, path='/')
    text = load_json(lang)
    show_ads = is_mobile()
    return dict(image=None, video=None, msg=None, show_ads=show_ads, text=text)


@post('/<lang:re:[a-z][a-z]>/')
@view('page')
def dld_lang(lang):
    link_src = request.forms.get('link-src')
    link_src = re.search(r'instagram\.com/(p/)?([a-zA-Z]|[0-9]|[_-])+', link_src)
    if link_src != None:
        link_src = link_src.group(0)
    # else:
    #     text = load_json(lang)
    #     show_ads = is_mobile()
    #     return dict(image=None, video=None, msg=None, show_ads=show_ads, text=text)
    link = link_src.split('/')[-1]
    return redirect('download/'+link)


@post('/<lang:re:[a-z][a-z]>/download/<url>')
@post('/<lang:re:[a-z][a-z]>/download/<url>/')
@get('/<lang:re:[a-z][a-z]>/download/<url>')
@get('/<lang:re:[a-z][a-z]>/download/<url>/')
@view('page')
def download_lang(lang, url):
    if change_lang():
        response.set_cookie('change_lang', 'False', path='/')
        lang_changed = get_user_lang()
        return redirect('/download/'+url)
    response.set_cookie('lang', lang, path='/')
    link_src = 'https://instagram.com/p/'+url
    img, video, msg = get_data(link_src)
    text = load_json(lang)
    show_ads = is_mobile()
    return dict(image=img, video=video, msg=msg, show_ads=show_ads, text=text)

#######################
### 404
#######################

@error(404)
@view('page')
def error404(error):
    text = load_json()
    show_ads = is_mobile()
    return dict(image=None, video=None, msg='404', show_ads=show_ads, text=text)

application = default_app() # comment to run on local
#run(host='localhost', port=2023, reload=True, debug=True) # uncomment to run on local
