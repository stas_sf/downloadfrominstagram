function CheckLink() {
  var re = /instagram\.com\/(p\/)?[a-zA-Z0-9]+/;
  var link = document.getElementById('link-src');
  var linkText = link.value;
  var valid = re.test(linkText);
  var wrong_link = document.getElementById('wrong-link');
  var existMsg = wrong_link.classList.contains('wl_show');
  if (!valid) {
    if (!existMsg) {
      wrong_link.classList.add('wl_show');
      wrong_link.classList.remove('wl_hide');
    }
  }
  else {
    wrong_link.classList.add('wl_hide');
    wrong_link.classList.remove('wl_show');
  }
  return valid;
};

function changeLang() {
  var sel = document.getElementById("language");
  var lang = sel.options[sel.selectedIndex].value;
  document.cookie = "change_lang=True; path=/";
  document.cookie = "lang="+lang+"; path=/";
  window.location.reload();
};
